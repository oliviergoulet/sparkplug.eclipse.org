---
title: Get Listed
about: Certification request
---

- [ ] Organization Name ("Organization") and, if applicable, URL:<br/>
  // Add here

- [ ] Product Name, Version and download/product information URL:<br/>
  // Add here

- [ ] Product logo URL (mandatory):<br/>
  // Add here

- [ ] Specification Version and download URL:<br/>
   // Add here

- [ ] Sparkplug conformance profiles covered by the product<br/>
  - [ ] Sparkplug Edge Node<br/>
  - [ ] Sparkplug Host Application<br/>
  - [ ] Sparkplug Compliant MQTT Server<br/>
  - [ ] Sparkplug Aware MQTT Server<br/>

- [ ] TCK Version, digital SHA-256 fingerprint and download URL:<br/>
  // Add here

- [ ] Public URL of TCK Results Summary:<br/>
  // Add here

- [ ] Summary of the environment used for certification: devices, software components, operating systems, cloud services, etc.<br/>
  // Add here or provide URL

- [ ] By checking this box I acknowledge that the Organization I represent accepts the terms of the [EFTL](https://www.eclipse.org/legal/tck.php).

- [ ] By checking this box I attest that all TCK requirements have been met, including any compatibility rules.

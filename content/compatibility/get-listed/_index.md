---
title: "Get Listed"
headline: "GET LISTED"
header_wrapper_class: 'header-sparkplug-get-listed text-left'
description: "Get listed on Sparkplug"
keywords: ["eclipse", "eclipse Sparkplug", "get listed"]
layout: "single"
container: "container-fluid get-listed-page"
featured_footer_class: get-listed-footer
hide_sidebar: true
hide_page_title: true
---

{{< compatibility/get-listed/steps >}}

{{< compatibility/get-listed/benefits >}}

{{< grid/div class="container get-listed-text-container" >}}
The Sparkplug Compatibility Program aims to provide integrators and end-users with an easy way to procure devices and software products that are fully compatible with the Sparkplug specification.

If you are a technology provider, getting your products listed showcases your commitment to Sparkplug and makes you a participant in a thriving marketplace. If you are interested in featuring your product here, follow the steps below.

By joining the compatibility program, you will gain the confidence of current and future adopters of Sparkplug by proving your products meets stringent quality and interoperability requirements. In turn, this will build your brand and expand your market reach.

If you have any questions, please email <sparkplug@eclipse.org>.

## Download, Run and Pass the TCK
The first step to getting your products listed is to download the Sparkplug Technology Compatibility Kit (TCK) and use it to certify that your products comply with the specification.

The TCK is open-source and [developed on GitHub](https://github.com/eclipse/sparkplug/tree/master). However, you must run your tests with a binary version of the TCK licensed under the [Eclipse Foundation Technology Compatibility Kit License](https://www.eclipse.org/legal/tck.php) (TCK License) to submit a product listing request.

You will be able to download the TCK binaries from download.eclipse.org.

To claim compatibility with the Sparkplug specification, you must comply with the Eclipse Foundation TCK License.
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container" >}}
## Become a Member
Only members of the Eclipse Foundation and its Sparkplug working group can participate in the compatibility program.

Our membership prospectuses for the [Foundation](https://www.eclipse.org/membership/documents/membership-prospectus.pdf) and working group highlight the benefits of being a member.

The two memberships are separate and require the payment of annual fees determined according to your organization's worldwide revenue. Please refer to the following links to determine the fees applicable to your case.

-   [Eclipse Foundation membership fees](https://www.eclipse.org/membership/#tab-fees)
-   [Sparkplug working group membership fees](https://www.eclipse.org/org/workinggroups/eclipse_sparkplug_charter.php)

Your organization must be a contributing or strategic member of the Foundation to participate in the program.

Fill out the [online membership application form](https://membership.eclipse.org/application#sign-in) to get your membership request started. The process will require your organization to execute the following agreements:

-   [Eclipse Foundation Membership Agreement](https://www.eclipse.org/org/documents/eclipse_membership_agreement.pdf)
-   [Member Committer and Contributor Agreement](https://www.eclipse.org/legal/committer_process/EclipseMemberCommitterAgreement.pdf)
-   [Sparkplug Working Group Participation Agreement](https://www.eclipse.org/org/workinggroups/wgpa/sparkplug-working-group-participation-agreement.pdf)
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container" >}}
## Compatibility Trademark License Agreement
One of the main benefits of participation in the Sparkplug compatibility program is that you will be able to use the "Sparkplug Compatible" word mark and logo in your marketing activities related to your compatible products.

Beforehand, your organization needs to execute the Sparkplug Compatibility Trademark License Agreement, which governs the word mark and logo usage. Only Foundation and working group members can execute the agreement. A link to the text of the agreement will be added to this page soon.

Execution of the agreement is mandatory for participation in the compatibility program.

Sparkplug®, Sparkplug Compatible and the Sparkplug Logo are trademarks of the Eclipse Foundation.
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container" >}}
## File a "Get Listed" Request
Once your organization is a member and has accepted the Trademark License Agreement, you can submit requests to get products listed on the compatible products page.

Listing requests will be managed through a public GitHub repository. You need to submit a separate request for every product you wish to see on the page.

To initiate a request, you need to create a GitHub issue against the repository using the "Sparkplug compatible product" issue template. Running the TCK against a product will produce a report file. You need to expose this file on the Internet and provide the URL in your listing request.

Requests need to be approved by a committer of the Sparkplug specification project. The committer will take a close look at the test results and testing environment to ascertain the accuracy of the listing request. Please allow at least two weeks for processing. When approved by a committer, the request will be transferred to a Foundation webmaster, who will add the product to the list of compatible products.
{{</ grid/div >}}

{{< grid/div class="container get-listed-text-container margin-bottom-60" >}}
## Promote Your Compatible Product
After a listing request is approved and the product is listed on the Sparkplug website, the only thing left to do is marketing. The Foundation publishes a Sparkplug Brand Usage Handbook that guides members to use the relevant trademarks.

Please note TCK results are valid only for a specific version of a product and of the Sparkplug specification. You will need to submit new listing requests if new versions of the Sparkplug specification are published or for every new major version of your product.
{{</ grid/div >}}

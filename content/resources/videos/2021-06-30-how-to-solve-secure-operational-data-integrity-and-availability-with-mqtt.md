+++
date = "2021-06-30"
title = "How to Solve Secure Operational Data Integrity and Availability with MQTT"
link = "https://cirrus-link.com/videos/webinar-how-to-solve-secure-operational-data-integrity-and-availability-with-mqtt/"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "https://i.vimeocdn.com/video/1177691974-55c6ba4e7cd96532a95aa58a60d6e483061343c9e47195945f84d9e179873df2-d?mw=720"
tags = [ "video", "open source", "Eclipse", "sparkplug", "MQTT"]
categories = ["video"]
+++

To meet the demand for operational data to the business enterprise and maintain the data integrity for security, Cirrus Link Solutions will discuss an easy-to-use approach using MQTT.

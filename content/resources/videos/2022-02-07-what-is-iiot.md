+++
date = "2022-02-07"
title = "What is IIoT"
link = "https://cirrus-link.com/videos/what-is-iiot/?wvideo=1efr7anfm9"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "https://embedwistia-a.akamaihd.net/deliveries/e84d1f5d816c39a4b0c270cc9c94aa54.jpg?image_crop_resized=720x405"
tags = [ "video", "open source", "Eclipse", "sparkplug", "IIoT"]
categories = ["video"]
+++

Travis Cox from Inductive Automation breaks down IIoT (the Industrial Internet of Things).

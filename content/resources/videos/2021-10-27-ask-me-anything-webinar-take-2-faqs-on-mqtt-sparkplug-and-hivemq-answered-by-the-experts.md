+++
date = "2021-10-27"
title = "Ask Me Anything Webinar: Take 2 | FAQs on MQTT, Sparkplug, & HiveMQ Answered by the Experts"
link = "https://www.youtube.com/watch?v=FWqkGZ5oZfU"
link_class  = "eclipsefdn-video"
tags = [ "video", "open source", "Eclipse", "sparkplug", "MQTT", "HiveMQ"]
categories = ["video"]
+++

About this webinar: In this second edition of HiveMQ webinar, ‘Ask Me Anything’ About MQTT: Take 2',  Dominik Obermaier, CTO and Co-Founder at HiveMQ, and Florian Raschbichler, Head of Support at HiveMQ, and Kudzai Manditereza, Technology Communicator & Founder of Industry40.tv, answered some of the most pressing questions around MQTT 5, Sparkplug specification, MQTT Security, HiveMQ Cloud, IIoT use cases, etc.

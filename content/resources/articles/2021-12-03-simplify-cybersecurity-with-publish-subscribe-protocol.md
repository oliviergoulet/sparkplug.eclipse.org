+++
date = "2021-12-03"
title = "Simplify Cybersecurity with Publish-subscribe Protocol"
link = "https://www.controlglobal.com/articles/2021/simplify-cybersecurity-with-publish-subscribe-protocol"
+++

Michigan-based Waterford Township is upgrading its water/wastewater controls and SCADA system with MQTT publish-subscribe protocol running on Opto 22's groov EPIC controllers.
